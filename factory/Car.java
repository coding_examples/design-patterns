package factory;

public class Car {

    // Note that these two member variables are interfaces.
    private Engine engine;
    private Steering steering;

    // Note that the constructor parameters are also interfaces.
    // Can you think why this might be?
    public Car(Engine engine, Steering steering) {
        this.engine = engine;
        this.steering = steering;
    }
    
}
