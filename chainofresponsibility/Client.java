package chainofresponsibility;

public class Client {

    public static void main(String[] args) {
        Server server = new Server();
        Request request = new Request("azurediamond;hunter2;admin");
        try {
            boolean success = server.handleRequest(request);
            
            if (success)
                System.out.println("Request handled successfully!");
            else
                System.out.println("Request failed!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
