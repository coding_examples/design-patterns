package chainofresponsibility;

public class RoleMiddleware extends Middleware {

    @Override
    public boolean handle(Request request) {
        String data = request.getData();
        if(data.split(";")[2].equals("admin")) {
            if (next != null) {
                return next.handle(request);
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
    
}
