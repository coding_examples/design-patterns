package chainofresponsibility;

public abstract class AbstractServer {

    protected Middleware middleware;

    protected void addMiddleware(Middleware middleware) {
        this.middleware = middleware;
    }

    protected boolean triggerMiddleware(Request request) {
        return middleware.handle(request);
    }
    
}
