package chainofresponsibility;

public class LoggingMiddleware extends Middleware {

    @Override
    public boolean handle(Request request) {
        String data = request.getData();
        String user = data.split(";")[0];
        
        System.out.println("[DEBUG] Accessed by user: " + user);

        if (next != null) {
            return next.handle(request);
        } else {
            return true;
        }
    }
    
}
