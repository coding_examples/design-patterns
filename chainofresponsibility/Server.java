package chainofresponsibility;

public class Server extends AbstractServer {

    public Server() {
        // Configure the chain somewhere
        Middleware authMiddleware = new AuthMiddleware();
        Middleware roleMiddleware = new RoleMiddleware();
        Middleware loggingMiddleware = new LoggingMiddleware();

        // TODO: Fill in code here.
        // Hint: Use the chain() method to connect the individual pieces.

        // Make sure the reference to the first element in chain is maintained
        super.addMiddleware(authMiddleware);
    }
    
    public boolean handleRequest(Request request) throws Exception {
        
        // Trigger middleware
        if (!triggerMiddleware(request)) {
            throw new Exception("Middleware processing caught an exception.");
        }

        // .. implementation of method body

        System.out.println("[DEBUG] Server: Data verified through middleware. Processing request..");

        return true;
    }
}
