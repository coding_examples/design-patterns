package chainofresponsibility;

public abstract class Middleware {

    protected Middleware next;

    public Middleware chain(Middleware m) {
        // TODO: Fill in code here.
    }

    public abstract boolean handle(Request request);
    
}
