package singleton;

public class Singleton {
    
    // Note the use of "static"
    private static Singleton INSTANCE = null;

    // Prevent constructor from being called
    private Singleton() {}

    // Note the use of "static". 
    // This is so that we can call this method with constructing it.
    public static Singleton getInstance() {
        // TODO: Fill in code here.
    }

    // Class body

    public void methodA() {
        // .. some code implementation
    }

}