package singleton;

public class Client {
    public static void main(String[] args) {
        // Singleton s = new Singleton(); // Error!
        Singleton s1 = Singleton.getInstance();
        System.out.println(s1);
        
        Singleton s2 = Singleton.getInstance();
        System.out.println(s2);
    }
}
