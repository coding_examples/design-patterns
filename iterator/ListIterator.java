package iterator;

public class ListIterator<E> implements Iterator<E> {

    private Object[] data;
    private int cursor;
    private int index = -1;

    public ListIterator(Object[] data) {
        this.data = data;
    }

    @Override
    public boolean hasNext() {
        return cursor != data.length;
    }

    @Override
    @SuppressWarnings("unchecked")
    public E next() {
        // TODO: Fill in code here.
    }
    
}
