package iterator;

public class Client {
    public static void main(String[] args) {
        Integer[] arr = new Integer[]{1,2,3,4,5};
        ListContainer<Integer> container = new ListContainer<Integer>(arr);
        Iterator<Integer> it = container.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }
}
